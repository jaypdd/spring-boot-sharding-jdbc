package com.jaypdd.shardingjdbc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 读写分离测试
 */
@SpringBootApplication
@Slf4j
public class ShardingJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShardingJdbcApplication.class, args);
		log.info("========================MySQL读写分离Sharding-Jdbc========================");
	}

}
