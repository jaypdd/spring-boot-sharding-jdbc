package com.jaypdd.shardingjdbc.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jaypdd.shardingjdbc.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  用户持久层接口
 * </p>
 *
 * @ClassName UserDao
 * @Description TODO
 * @Author jaypdd
 * @Date 2021/1/24 17:17
 * @Version v1.0.0
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
}
