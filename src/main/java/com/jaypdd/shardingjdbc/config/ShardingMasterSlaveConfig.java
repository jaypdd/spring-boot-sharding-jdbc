package com.jaypdd.shardingjdbc.config;

import com.zaxxer.hikari.HikariDataSource;
import io.shardingjdbc.core.api.config.MasterSlaveRuleConfiguration;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  主从说数据库读写分离配置类
 * </p>
 *
 * @ClassName ShardingMasterSlaveConfig
 * @Description TODO
 * @Author jaypdd
 * @Date 2021/1/24 16:23
 * @Version v1.0.0
 */
@Data
@ConfigurationProperties(prefix = "sharding.jdbc")
public class ShardingMasterSlaveConfig {

	/**
	 * 存放本地多个数据源
	 */
	private Map<String, HikariDataSource> dataSources = new HashMap<>();

	/**
	 * 主从数据库规则配置类
	 */
	private MasterSlaveRuleConfiguration masterSlaveRule;
}
