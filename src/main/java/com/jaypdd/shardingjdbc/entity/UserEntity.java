package com.jaypdd.shardingjdbc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *  测试用户实体类
 * </p>
 *
 * @ClassName UserEntity
 * @Description TODO
 * @Author jaypdd
 * @Date 2021/1/24 17:15
 * @Version v1.0.0
 */
@Data
@ToString
@TableName("user")
@Accessors(chain = true)
public class UserEntity implements Serializable {

	/**
	 * 用户id
	 */
	@TableId(type = IdType.AUTO)
	private Integer userId;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改时间
	 */
	private Date updateTime;
}
