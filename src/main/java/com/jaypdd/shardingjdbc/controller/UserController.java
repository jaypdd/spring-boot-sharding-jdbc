package com.jaypdd.shardingjdbc.controller;

import com.jaypdd.shardingjdbc.entity.UserEntity;
import com.jaypdd.shardingjdbc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  用户管理控制器
 * </p>
 *
 * @ClassName UserController
 * @Description TODO
 * @Author jaypdd
 * @Date 2021/1/24 17:20
 * @Version v1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

	@Resource
	private UserService userService;

	/**
	 * 获取用户信息列表
	 * @return  返回用户信息列表
	 */
	@GetMapping("/getUserList")
	public Object getUserList() {
		return userService.list();
	}

	/**
	 * 添加用户信息
	 */
	@GetMapping("/saveUser")
	public Object saveUser() {
		UserEntity userEntity = new UserEntity();
		userEntity.setUserName("user");
		userEntity.setPassword("a31afc8669cc3bfca90f0d374b2f34c9" );
		userEntity.setCreateTime(new Date());
		userService.save(userEntity);
		userEntity.setUserName(userEntity.getUserName() + userEntity.getUserId());
		userService.updateById(userEntity);
		try {
			return "user" + userEntity.getUserId();
		} catch (Exception e) {
			log.error("Slave DataBase Connection Fail!", e);
			return "Slave DataBase Connection Fail!!!";
		}
	}

}
