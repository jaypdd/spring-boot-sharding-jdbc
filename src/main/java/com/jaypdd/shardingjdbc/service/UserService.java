package com.jaypdd.shardingjdbc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jaypdd.shardingjdbc.entity.UserEntity;
import lombok.Data;

/**
 * <p>
 *  用户业务层接口
 * </p>
 *
 * @ClassName UserService
 * @Description TODO
 * @Author jaypdd
 * @Date 2021/1/24 17:18
 * @Version v1.0.0
 */
public interface UserService extends IService<UserEntity> {
}
