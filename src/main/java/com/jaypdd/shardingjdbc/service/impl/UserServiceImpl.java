package com.jaypdd.shardingjdbc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jaypdd.shardingjdbc.dao.UserDao;
import com.jaypdd.shardingjdbc.entity.UserEntity;
import com.jaypdd.shardingjdbc.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  用户业务层接口实现类
 * </p>
 *
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author jaypdd
 * @Date 2021/1/24 17:18
 * @Version v1.0.0
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {
}
